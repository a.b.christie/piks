# files
The `.bin` files are copies of the ARM **k3s** binaries that are
placed in the pi-user's home directory. Simply remove the `.bin` and
the preceding version number, e.g. `k3s-armhf-0.3.0.bin`becomes
`k3s-armhf`.
