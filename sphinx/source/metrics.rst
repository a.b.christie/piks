#######
Metrics
#######

Starting with k3s v0.4.0 you can install metrics on the server. The recipe
is contained in the k3s repository but the metrics image being run is
not compatible with the ARM. To solve the problem a snapshot of the
recipe has been inserted here, in the **metrics** role's files.

As well as fixing the metrics image architecture, labels have been added
to each object to simplify removal.

To install metrics, run::

    $ ansible-playbook playbooks/k3s/add-metrics.yml

To see node and pod metrics you can run the following commands from your
control machine::

    $ kubectl top node
    $ kubectl top pod --all-namespaces

You will need to wait a minute or two before metrics become available. Until
then you'll be greeted with ``error: metrics not available yet``.

You can remove metrics from the k3s cluster with::

    $ ansible-playbook playbooks/k3s/remove-metrics.yml
