#####################
Configuring a cluster
#####################

So, you've got all your Raspberry Pi hardware setup and available so that you
can **ssh** to each device. This project's Ansible roles and playbooks
might come in handy to automatically configure the hardware as a cluster.
The playbooks configure one device as a server and others as agent devices.

You will need: -

-   Ansible (ideally a recent **v2.7** version)

The expectation is that: -

-   ``k3s-armhf`` is installed in the the pi user's home directory on each device

Preparing for Ansible
=====================
Move to the ``ansible`` directory and edit the ``inventory.yml`` file,
which identifies each device to configure. The built-in inventory
assumes: -

-   The device to be used as the k3 server is **k3s-server.local**
-   There are four agents, **k3s-01.local**, **k3s-02.local**, =
    **k3s-03.local** and **k3s-04.local**.
-   The device ssh user is **pi**

Edit the ``ansible_user`` and ``hosts`` in your ``inventory.yml``
to match your hardware.

When you think you're ready you can *ping* your hardware to make sure
that Ansible can communicate with each device by running the following::

    $ ansible all -m ping

If that works you should be able to run any of the following playbooks.

Playbooks
=========
There are a number of playbooks, which use the project's **server** and
**agent** roles. We'll explore them here in the order they'd typically be used: -

-   Start
-   Check
-   Reboot
-   Shutdown

Start
-----
This playbook starts the server on the host listed in your **k3s-server**
inventory group.

From the ``ansible`` directory, run::

    $ ansible-playbook playbooks/k3s/start.yml


Once started, the playbook waits for the kubernetes API to become
available and also collects a list of *agents* considered **Ready**.
If you want to conveniently use ``helmm`` on your host review our
:doc:`helm` documentation.

After the server has started the playbook then starts each agent, waiting
for the agents to be recognised by the server before finishing.

The whole process can take a minute or two.

When the playbook is complete your server will be running and all your agents
should be attached.

Check
-----
This playbook does not alter the cluster but simply checks that it *appears*
to be in a generally good (healthy) state. It checks a number of things: -

-   That the server API port (6443) is open
-   Each agent in the inventory is known to the server and is in a **Ready**
    state
-   No Pods are in a **Failed** or **CrashLoopBackOff** state

It's basically a re-assuring *sanity check* that you can run occasionally
to check the cluster's health.

From the ``ansible`` directory, run::

    $ ansible-playbook playbooks/k3s/check.yml


Reboot
------
This just reboots the cluster, followed by a re-run of the tasks required
to start the cluster.

It exists simply to *bounce* the hardware during test and development.

The server is rebooted after the agents have been rebooted and then the
*start* tasks are re-executed.

From the ``ansible`` directory, run::

    $ ansible-playbook playbooks/k3s/reboot.yml


Shutdown
--------
This playbook shuts down each *agent*, waiting for them to stop before
shutting-down the server. It's just a quick and simple way to power your
cluster down so that you can safely disconnect power.

The server is stopped after the agents have been stopped.

From the ``ansible`` directory, run::

    $ ansible-playbook playbooks/k3s/shutdown.yml


Playbook variables
==================
A number of variables that control playbook behaviour are exposed
in the following locations: -

-   ``group_vars/all.yml``
-   ``roles/server/defaults/main.yml``

If you've setup your hardware as described the default values
should be sufficient for your cluster.

To avoid repeating myself the variables will not be described here.
Each of the variables is accompanied by substantial documentations so it
would be beneficial to familiarise yourself with what's available.
