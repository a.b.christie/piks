####################
Using Helm with PiKs
####################

`Helm`_ is compatible with the k3s distribution as long as you install an
ARM-based **tiller**. If you want to use helm the ansible **start** playbook
in this project conveniently supports automatic initialisation of it in your
RPi cluster.

First, you will need an installation of ``helm`` and the kubernetes CLI
(``kubectl``) on the Ansible control machine. If you're using OSX this is
easily achieved using `brew`_::

    $ brew install kubernetes-cli kubernetes-helm

Once installed you can declare that you want to use helm on your cluster
when you next run the **start** playbook by setting a couple of variables,
that are documented in ``ansible/group_vars/all.yml``::

    fetch_kube_config
    initialise_helm

Both of these need to be set [#f1]_ to ``yes`` before running (or re-running)
the **start** playbook. Or, you can add them to the playbook's command-line::

    $ cd <project-root>/ansible
    $ ansible-playbook playbooks/k3s/start.yml \
        -e fetch_kube_config=yes \
        -e initialise_helm=yes

The playbook initialises helm with a tiller-specific *service account*,
deploys an ARM-compliant **tiller** [#f2]_, waits until tiller gets to a
*Running* state before pulling the kubernetes configuration file form the
server device to your control machine's ``~/.kube`` directory.

When the playbook's finished you can run helm against your cluster
from your control machine using the ``KUBECONFIG`` environment variable::

    $ export KUBECONFIG=~/.kube/k3s.yaml
    $ helm list

.. _brew: https://brew.sh
.. _helm: https://helm.sh

.. rubric:: Footnotes

.. [#f1] The ``fetch_kube_config`` variable pulls the config file from
         the cluster into ``~/.kube/k3s-config`` on your control machine.
         If you are running other k3s clusters ans use the same file its config
         will be lost if you don't take preventative action.

.. [#f2] Assuming that your Raspberry Pi cluster is running a 32-bit ARM OS
         and can run ``arm32v7`` images.
