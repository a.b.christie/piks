####
PiKs
####

A Raspberry Pi/Kubernetes Experiment hosted on `GitLab`_.

This is a simple project that experiments with the lightweight
version of `Kubernetes`_ on the Raspberry Pi and has been inspired
by Mark Abrams' `article`_.

We take Mark's article and configure hardware and add Ansible roles
and playbooks to configure and deploy kubernetes to a network of
Raspberry Pi hardware.

This project's documentation is available on `Read The Docs`_ and
the source in the GitLib `piks`_ project.

.. _article: https://medium.com/@mabrams_46032/kubernetes-on-raspberry-pi-c246c72f362f
.. _gitlab: https://gitlab.com/a.b.christie/piks
.. _kubernetes: https://github.com/rancher/k3s
.. _piks: https://gitlab.com/a.b.christie/piks
.. _read the docs: https://piks.readthedocs.io

..  toctree::
    :maxdepth: 1
    :caption: Contents:

    preparing-the-hardware
    configuring-a-cluster
    helm
    metrics
