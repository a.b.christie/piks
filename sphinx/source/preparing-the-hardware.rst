######################
Preparing the hardware
######################
The project uses Raspberry Pi hardware with built-in WiFi and has
been tested with the following: -

-   Raspberry Pi 3 Model A+
-   Raspberry Pi 2 Model B V1.1 (with WiFi USB Dongle)
-   Raspberry Pi 3 Model B+
-   Raspberry Pi 3 Model B V1.2

The Raspberry Pi Zero W V1.1 uses the BCM2835 SoC device and is
an ARMv6 instructions set and is not likely to work. Ideally,
use Pi 3 devices, which have an ARMv8 or v7.

Software used:

-   A base OS (like **2019-06-20-raspbian-buster-lite**)
-   A flash utility (like **balenaEtcher** v1.5.51)
-   A k3s distribution (**v0.6.1** at the time of writing). A copy
    of the distribution used can be found in the project's `/files` directory
-   For cluster management, Ansible (ideally a recent release of **v2.7**)

Preparing an image
==================
The basic instructions are contained in Mark's `article`_, with a few
adjustments discussed here, listed against the sections in the original
article.

WiFi
----
I could not connect to the RPi using his **wpa_supplicant.conf** example.
Instead I needed to use a fuller file and making sure the network block
was correctly formatted. Anyway, the following worked for me::

    country=gb
    update_config=1
    ctrl_interface=/var/run/wpa_supplicant

    network={
      scan_ssid=1
      ssid="Wensleydale"
      psk=<encrypted key>
    }

-   It's important to specify the ``ssid`` and ``psk`` in quotes (``"``).
    If the psk is the result of using ``wpa_passphrase`` then
    you can skip the quotes on its value
-   Run ``wpa_passphrase MYSSID PASSPHRASE`` on an RPi where ``MYSSID`` is
    your wireless id (i.e. **Wensleydale** in my case) and the
    ``PASSPHRASE`` is your wireless passphrase. The output is a block you
    can paste into your **wpa_supplicant.conf**

First boot (extras)
-------------------

Add an ssh key-pair
^^^^^^^^^^^^^^^^^^^
You might want to add a public SSH key to simplify ssh connection in the
future, avoiding a password prompt. Also an essential step if you also want
to run the Ansible playbooks in this project, which relies on unprompted ssh
access.

If you have a key-pair you use then now'd be the time to create an ``~/.ssh``
directory (mode **0700**) with an ``authorised_keys`` file (**644**)
with your chosen public key in it.

With that done and the RPi running you should be able to use
``ssh-agent`` and the private part of your key-pair to connect
without a prompt::

    $ eval $(ssh-agent)
    $ ssh-add private-key-file
    $ ssh pi@k2s.local


Change pi's default password
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use this opportunity to change the default password for the pi user.
You can do this with ``passwd`` or via the ``sudo raspi-config`` utility.

Add an alias for kubectl
^^^^^^^^^^^^^^^^^^^^^^^^
While you're in the first boot add an alias to the end of the ``.bashrc``
file to simplify the local execution of ``kubectl`` commands. Add something
like this to the file::

    # Kubernetes aliases
    alias kubectl='sudo ./k3s-armhf kubectl'

Setup hostname configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If you have changed the hostname of the RPi then you must replace
the name for the local host in ``/etc/hosts``, It will be ``raspberrypi``.
So, if you've called the host ``k3s`` then you should have the
following in your ``/etc/hosts`` file::

    127.0.?.?   k3s

If you do not, you will not be able to start the k3s server
as it expects the localhost to match the hostname.

Do this to both ``/etc/hosts`` and the ``/etc/hostname`` file, which
will require you to use ``sudo``.

Add the k3s runtime binaries
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In case it is not obvious these go into the user's home directory
(i.e. ``/home/pi``). As you're logged in as **pi** you can run::

    curl -L -O https://github.com/ibuildthecloud/k3s/releases/download/v0.6.1/k3s-armhf
    chmod 755 k3s-arm*

Auto-expand
^^^^^^^^^^^
Using the ``sudo raspi-config`` utility again.

Mark's instructions are handy but it's not clear from the instructions what
image size you create for the 2nd run but the second image you create is
exactly the same size as the first (i.e. about 1.8Gi in my case).

Do not bother doing a second ``sudo fdisk /dev/disk2`` because you'll get
told it's an 8GiB image, which is not what you want.

But be aware that this expansion only works for SD cards
of the same size/type. If you've mastered an a Samsung 8GiB card then the
expandfs image you create as part of the **Create the second image** step
will only properly expand if you use another Samsung 8GiB card.
And it certainly won't expand to 32GiB on a 32GiB card.

Auto auto-expansion is great, but you probably need to keep images
for each type/size of card: -

-   k3s-expandfs-8g.img
-   k3s-expandfs-16g.img

Personally, as I'm not generating a lot of cards I use the base image
and expand it when I first use it. It saves a lot of time if you
just want a few cards and is a little more reliable. So I don't
both with this step and instead *flash* the base image and then change
the hostname and expand using **sudo raspi-config** when I first use the card.

Boot Up
=======
On first boot your device should have ssh available at ``pi@k3s.local``.

Before you do anything else, change both ``/etc/hostname`` and
``/etc/hosts`` to ensure you have a name that won't clash with other RPis
that you'll want to add. My base image hostname is ``k3s`` so I name
the server ``k3s-server`` and the individual nodes/agents ``k3s-01`` and
``k3s-02`` and so-on.

Once renamed ``sudo reboot`` the card and re-connect using the new hostname.

I suspect there's a typo in the initial examples. The server runs in a blocking
mode so need to be run *in the background* (with a trailing ``&``)::

    $ sudo ./k3s-armhf server &

Remember that you must have adjusted ``/etc/hosts`` to match the card's actual
hostname or the server will fail to start with errors.

The startup can take a minute or two. Be patient. The log finally settles down
with a few **waiting for node k3s: nodes "k3s" not found** INFO messages as
the initial containers are launched.

When these warnings about k3s not being found stop then you're pretty-much
ready and can follow the server startup with something like::

    $ kubectl get pods --all-namespaces

To see something like::

    NAMESPACE     NAME                             READY   STATUS      RESTARTS   AGE
    kube-system   coredns-7748f7f6df-8l4g4         1/1     Running     0          3m16s
    kube-system   helm-install-traefik-csftm       0/1     Completed   0          3m14s
    kube-system   svclb-traefik-76b88d8f8f-6v2nf   2/2     Running     0          81s
    kube-system   traefik-6876857645-5w2v7         1/1     Running     0          80s

Anyway, that's as far as I go with *manual* commands. From here I create
Ansible playbooks to start and manage the cluster.

With your hardware ready and the ability to ssh to each device it's time
to switch to the :doc:`running-the-cluster` guide so that you can
start and manage your hardware using Ansible and *Infrastructure as Code*
techniques ... it's time to embrace automation.

OSX commands
============
A small cheat-sheet of handy OSX commands.

#.  The SD card is likely to be mounted as ``/Volumes/boot``

#. ``diskutil list`` will expose the mounted device names

#.  You will probably need to use ``sudo fdisk /dev/disk2`` as the ``-l``
    option is not present in the OSX command.

#. The typical command to create a base image on OSK is likely to be
   ``sudo dd if=/dev/disk2 of=k3s-base.img bs=512 count=3645440``

.. _article: https://medium.com/@mabrams_46032/kubernetes-on-raspberry-pi-c246c72f362f
.. _kubernetes: https://github.com/rancher/k3s
