---

# A test playbook to check the state of the system.
# This basically involves checking the server is up
# and that all the agents appear 'Ready'.

- name: Check server API port
  wait_for:
    port: 6443
    timeout: 4
  changed_when: false

- include_tasks: get-agents.yml

- name: Fail if agents are not Ready
  fail:
    msg: Agent {{ item }} is not considered Ready by the Server
  when:
  - "'k3s-agents' in groups"
  - item|regex_replace('\\..*$') not in ready_agents
  with_items: "{{ groups['k3s-agents'] }}"

- name: Fail if server agent is not Ready
  fail:
    msg: Server Agent {{ item }} is not considered Ready by the Server
  when:
  - server_is_agent
  - item|regex_replace('\\..*$') not in ready_agents
  with_items: "{{ groups['k3s-server'] }}"

# Now check the state of deployed pods.
# They should all be one of the 'good' states.

- name: Get pod states (all namespaces)
  shell: >-
    {{ kubectl }} get po --all-namespaces --no-headers
    -o=custom-columns=STATUS:.status.phase
  register: pod_result
  changed_when: false

- name: Fail if pod states bad
  fail:
    msg: A pod is not in a Good state
  when: item in poor_pod_states
  with_items: "{{ pod_result.stdout_lines }}"
