---
Title:      A Raspberry Pi/Kubernetes Experiment
Author:     Alan Christie
Date:       2 July 2019
---

[![pipeline status](https://gitlab.com/a.b.christie/piks/badges/master/pipeline.svg)](https://gitlab.com/a.b.christie/piks/commits/master)
[![Documentation Status](https://readthedocs.org/projects/piks/badge/?version=latest)](https://piks.readthedocs.io/?badge=latest)

# PiKs
A Raspberry Pi/Kubernetes Experiment.

This is a simple project that experiments with the lightweight
version of [Kubernetes] on the Raspberry Pi and has been inspired
by Mark Abrams [article].

We describe the hardware and provide Ansible playbooks for
the setup and management of a Raspberry Pi-based cluster.

The project uses Raspberry Pi hardware with a mix of built-in WiFi
and USB-based WiFi dongles and has been tested with the following devices: -

-   Raspberry Pi 3 Model A+
-   Raspberry Pi 2 Model B V1.1 (with WiFi USB Dongle)
-   Raspberry Pi 3 Model B+
-   Raspberry Pi 3 Model B V1.2

Documentation, written using [Sphinx] can be found in the sphinx directory
and online at [Read The Docs].

## kubectl 'cheat sheet'
Some commands I find useful...

-   Set the active namespace:
    `kubectl config set-context --current --namespace=patterneer`

---

[article]: https://medium.com/@mabrams_46032/kubernetes-on-raspberry-pi-c246c72f362f
[kubernetes]: https://github.com/rancher/k3s
[read the docs]: https://piks.readthedocs.io
[sphinx]: https://pypi.org/project/Sphinx/
